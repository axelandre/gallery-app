# Cahier des charges du projet
* **Upload de photos** : `$_FILES` et sécurité
* **Gestion de comptes** : `$_SESSION` et `$_COOKIE`, BDD, sécurité, user ≠ admin
* **Notes + commentaires** : BDD + formulaires
* **Tags + catégories** : AJAX
* **Profil utilisateur** : fonction de suivie