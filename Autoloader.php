<?php
/**
 * Created by PhpStorm.
 * User: axelandre
 * Date: 17/01/2019
 * Time: 10:36
 */

class Autoloader
{
    public function __construct()
    {
        function chargerClasses($className)
        {
            require "class/" . $className . ".class.php";
        }

        spl_autoload_register('chargerClasses');

    }



}