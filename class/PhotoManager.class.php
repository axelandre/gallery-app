<?php
/**
 * Created by PhpStorm.
 * User: axelandre
 * Date: 02/03/2019
 * Time: 17:42
 */

class PhotoManager
{
    public function __construct()
    {
    }

    public static function add(Photo $photo)
    {
        $req = BaseDeDonnees::getConnection()->prepare("INSERT INTO Photos (titre,image,description,dateCreation,id_user_create,credit,lieu) VALUES (:titre,:image,:description,NOW(),:id_user_create,:credit,:lieu)");
        $req->bindValue(":titre", $photo->getTitre());
        $req->bindValue(":image", $photo->getImage());
        $req->bindValue(":credit", $photo->getCredit());
        $req->bindValue(":lieu", $photo->getLieu());
        $req->bindValue(":description", $photo->getDescription());
        $req->bindValue(":id_user_create", $photo->getIdUserCreate());
        $req->execute();
        $photo->hydrate([
            "id" => BaseDeDonnees::getConnection()->lastInsertId()
        ]);
        return $photo;
    }

    public static function delete($id)
    {
        BaseDeDonnees::getConnection()->prepare("DELETE FROM Photos WHERE id = $id")->execute();
    }
}