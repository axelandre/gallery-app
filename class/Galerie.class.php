<?php
/**
 * Created by PhpStorm.
 * User: pierrelefevre2
 * Date: 28/02/2019
 * Time: 11:15
 */

class Galerie
{
    protected $id;
    protected $titre;
    protected $description;
    protected $dateCreation;
    protected $idUserCreate;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * @param mixed $titre
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * @param mixed $dateCreation
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;
    }

    /**
     * @return mixed
     */
    public function getIdUserCreate()
    {
        return $this->idUserCreate;
    }

    /**
     * @param mixed $id_user_create
     */
    public function setIdUserCreate($idUserCreate)
    {
        $this->idUserCreate = $idUserCreate;
    }
    public function __construct($donnes)
    {
        $this->hydrate($donnes);
    }

    public function hydrate(array $donnees){
        foreach ($donnees as $key => $value){
            $method = "set".ucfirst($key);
            if(method_exists($this, $method)){
                $this->$method($value);
            }
        }
    }


}