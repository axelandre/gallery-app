<?php

/**
 * Created by PhpStorm.
 * User: axelandre
 * Date: 10/01/2019
 * Time: 10:50
 */
class BaseDeDonnees
{
    /**
     * BaseDeDonnees constructor.
     */

    protected $user = "root";
    /**
     * @var string
     */
    protected $pass = "root";
    protected $host = "localhost";
    protected $db_name = "galerie";
    /**
     * @var PDO
     */
    protected static $connection;

    public function __construct()
    {
        $connect = new PDO('mysql:host=' . $this->host . ';dbname=' . $this->db_name, $this->user, $this->pass);
        $this->setConnection($connect);
    }


    /**
     * @return PDO
     */
    public static function getConnection()
    {
        return self::$connection;
    }

    /**
     * @param PDO $connection
     */
    public function setConnection($connection)
    {
        self::$connection = $connection;
    }


    /**
     * @param $query
     * @return bool|PDOStatement
     */
    public static function query($query){
        return BaseDeDonnees::getConnection()->query($query);
    }

}