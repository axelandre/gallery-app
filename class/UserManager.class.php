<?php
/**
 * Created by PhpStorm.
 * User: axelandre
 * Date: 17/01/2019
 * Time: 09:24
 */

class UserManager
{
    private $bdd;

    /**
     * @param PDO
     */
    public function __construct()
    {
        $bdd = new BaseDeDonnees;
        $this->setBdd($bdd->getConnection());
    }

    public static function add(User $user)
    {
        $req = BaseDeDonnees::getConnection()->prepare("INSERT INTO user (email,mdp,pseudo,date_inscription) VALUES (:email,:mdp,:pseudo,NOW())");
        $req->bindValue(":email", $user->getEmail());
        $req->bindValue(":mdp", $user->getMdp());
        $req->bindValue(":pseudo", $user->getPseudo());
        $req->execute();
        $user->hydrate([
            "id" => BaseDeDonnees::getConnection()->lastInsertId()
        ]);
        return $user;
    }
    public static function delete($id)
    {
        BaseDeDonnees::getConnection()->prepare("DELETE FROM user WHERE id = $id")->execute();
    }
    public static function update($id,User $user)
    {
        $req = BaseDeDonnees::getConnection()->prepare("UPDATE user SET (email,mdp,pseudo,date_inscription) VALUES (:email,:mdp,:pseudo,NOW()) WHERE id = $id");
        $req->bindValue(":email", $user->getEmail());
        $req->bindValue(":mdp", $user->getMdp());
        $req->bindValue(":pseudo", $user->getPseudo());
        $req->execute();
        $user->hydrate([
            "id" => BaseDeDonnees::getConnection()->lastInsertId()
        ]);
        return $user;
    }
    public static function isEmailRegistered($donnees){
            $donnees;
            $req = BaseDeDonnees::query("SELECT email FROM user WHERE email = '$donnees'");
            if($req->rowCount() > 0){
                $_POST["message"]= "Déja";
                return true;
            }else{
                $_POST["message"]= "Déja enregistré";
                return false;

            }
    }
    public static function addUser()
    {
        if (isset($_POST["addUser"])) {
            if (isset($_POST["pseudo"]) && isset($_POST["email"]) && isset($_POST["password"]) && isset($_POST["confirmPassword"])) {
                if ($_POST["password"] === $_POST["confirmPassword"])
                    $user = new User([
                        "email"=>$_POST["email"],
                        "mdp"=>$_POST["password"],
                        "pseudo"=>$_POST["pseudo"],
                        ]);
                    if (self::isEmailRegistered($_POST["email"]) == false)
                    self::add($user);
                else
                $_POST["message"]= "Les mots de passes ne correpondent pas !";
                return false;
            }else{
                $_POST["message"]= "Les mots de passe ne sont pas identiques!";
                return false;
            }
        }else{
            $_POST["message"]= "Pas la bonne action";
            return false;
        }
    }
    public static function getAllUsers(){
        $req = BaseDeDonnees::query("SELECT id, email, pseudo, mdp, admin, date_inscription, actif FROM user");
        $results  = [];
        foreach ($req as $user){
            $user = new User([
                "email"=>$user["email"],
                "mdp"=>$user["mdp"],
                "pseudo"=>$user["pseudo"],
                "id"=>$user["id"],
                "dateInscription"=>$user["date_inscription"],
            ]);
            array_push($results,$user);
        }
        return $results;
    }
    public static function countAllusers(){
        $req = BaseDeDonnees::query("SELECT id, email, pseudo, mdp, admin, date_inscription, actif FROM user");
        return $req->rowCount();
    }

}