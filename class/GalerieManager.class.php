<?php
/**
 * Created by PhpStorm.
 * User: pierrelefevre2
 * Date: 28/02/2019
 * Time: 11:22
 */

class GalerieManager
{
    private $bdd;

    /**
     * @param PDO
     */
    public function __construct()
    {
        $bdd = new BaseDeDonnees;
        $this->setBdd($bdd->getConnection());
    }

    public static function add(Galerie $galerie)
    {
        $req = BaseDeDonnees::getConnection()->prepare("INSERT INTO galerie (titre,description,dateCreation,id_user_create) VALUES (:titre,:description,NOW(),:id_user_create)");
        $req->bindValue(":titre", $galerie->getTitre());
        $req->bindValue(":description", $galerie->getDescription());
        $req->bindValue(":id_user_create", $galerie->getIdUserCreate());
        $req->execute();
        $galerie->hydrate([
            "id" => BaseDeDonnees::getConnection()->lastInsertId()
        ]);
        return $galerie;
    }

    public static function delete($id)
    {
        BaseDeDonnees::getConnection()->prepare("DELETE FROM galerie WHERE id = $id")->execute();
    }
}